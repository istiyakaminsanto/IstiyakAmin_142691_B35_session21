<?php

namespace App\BookTitle;


use App\Model\Database as DB;
require_once("../../../../vendor/autoload.php");
//use App\Model\Database;

class Booktitle extends DB{
    public $id;
    public $book;
    public $author_name;

    public function __construct(){
        parent::__construct();
    }
}

$objBookTitle = new Booktitle();
