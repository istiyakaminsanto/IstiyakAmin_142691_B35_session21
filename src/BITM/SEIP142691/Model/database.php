<?php
namespace App\Model;

use PDO;
use PDOException;


//print_r(PDO::getAvailableDrivers());

class Database{
    public $host ="localhost";
    public $dbname ="atomic_project_b35";
    public $user="root";
    public $pass="";


    public function __construct()
    {


        try {

            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
            echo "connected succesfully";
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}

$objDatabase = new Database();