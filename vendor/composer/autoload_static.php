<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita4f1feab91cd5981dd8e0c57a849dc3f
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP142691',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita4f1feab91cd5981dd8e0c57a849dc3f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita4f1feab91cd5981dd8e0c57a849dc3f::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
